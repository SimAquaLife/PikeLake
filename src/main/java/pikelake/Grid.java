package pikelake;

import java.io.File;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.io.FileNotFoundException;

import pikelake.Cell;
import pikelake.pikes.Pike;
import pikelake.environment.Time;
import pikelake.environment.AreaMovement;
import pikelake.environment.FichierMarnage;

import fr.cemagref.simaqualife.pilot.Pilot;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.extensions.spatial2D.Grid2D;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;

/**
 * Classe qui initialise la grille de la simulation (lac et cellule non
 * accessible)
 *
 * @author Guillaume GARBAY
 * @version 1.0
 */
public class Grid extends Grid2D<Cell, Individual> {

    private String marnageInit = "513_4";
    private int cellnumber_2 = (gridWidth * gridHeight) - 2;

    /**
     * Constructeur de la classe Grid
     *
     * @param gridWidth Largeur totale de la grille
     * @param gridHeight Longueur totale de la grille
     * @param neighborsType Type de voisinnage utilise
     * @return /
     */
    public Grid(int gridWidth, int gridHeight, NeighborsType neighborsType) {
        super(gridWidth, gridHeight, neighborsType);
        // Auto-generated constructor stub
    }

    /**
     * Declaration + Initialisation de la grille avec les HSI de chaque cellule
     * Appelee lors de l'initialisation du programme
     *
     * @param pilot
     * @return /
     */
    @InitTransientParameters
    public void initTransientParameters(Pilot pilot) throws FileNotFoundException {

        StringTokenizer sLigne;
        @SuppressWarnings("unused")
        double hsiStd = 0, hsiMoy = 0;
        int idCell = 0, yPike = 0, xPike = 0;

        // Declaration grille vide
        grid = new Cell[gridWidth * gridHeight];

        // Initialisation de toute la grille avec hsi = -1
        for (int cptCell = 0; cptCell < (gridWidth * gridHeight - 1); cptCell++) {
            grid[cptCell] = new Cell(cptCell, -1);
        }

        marnageInit = FichierMarnage.dateMarnage[Time.jourMois][Time.mois][Time.heure];
        if (marnageInit == null) {
            marnageInit = "513_4";
        }

        // Lecture fichier contenant les HSI de toutes les mailles
        String filePath = "data/input/HSI/hsi_BRO" + Time.getSeason() + marnageInit + ".txt";
        Scanner scanner = new Scanner(new File(filePath));

    	// Initialisation de chaque cellule contenant HSI
        // On boucle sur chaque ligne detect�e
        String line = scanner.nextLine();
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();

            // Decoupage ligne : id, hsiStd, hsiMoy
            sLigne = new StringTokenizer(line);
            if (sLigne.hasMoreTokens()) {
                idCell = Integer.parseInt(sLigne.nextToken()) - 1;
            }
            if (sLigne.hasMoreTokens()) {
                hsiStd = Double.parseDouble(sLigne.nextToken());
            }
            if (sLigne.hasMoreTokens()) {
                hsiMoy = Double.parseDouble(sLigne.nextToken());
            }

    		// Conversion idCell en coordonnees (x, y) 
            // (x, y) avec les id de cellules de 0 � n-1
            yPike = (int) Math.floor(idCell / gridWidth);
            xPike = (int) idCell - (yPike * gridWidth);

            // Inversion des coordonnees en Y (place l'origine en bas � gauche)
            yPike = (gridHeight - 1) - yPike;
            // Conversion des coordonnees (x, y) en idCell 
            idCell = xPike + yPike * gridWidth;

            // Initialisation du hsi de la cellule(idCell)
            grid[idCell] = new Cell(idCell, hsiMoy);
        }
        scanner.close(); // Fermeture du fichier
    }

    /**
     * Procedure qui definit la valeur du HSI pour une cellule
     *
     * @param indexCell Indice de la cellule a traiter
     * @param hsiCell Valeur du HSI
     * @return /
     */
    public void setCell(int indexCell, double hsiCell) {
        grid[indexCell] = new Cell(indexCell, hsiCell);
    }

    /**
     * Procedure qui renvoie la grille actuelle en cours de simulation
     *
     * @param /
     * @return Grid Grille de la simulation
     */
    public Grid getGrid() {
        return this;
    }

    /**
     * Procedure qui ajoute un individu au groupe d'individus aquaNism
     *
     * @param ind L'individu a ajouter
     * @param group Groupe des individus
     * @return /
     */
    public void addAquaNism(Individual ind, AquaNismsGroup<?, ?> group) {
        super.addAquaNism(ind, group);
        ind.getPosition().addPike((Pike) ind);
    }

    /**
     * Procedure qui deplace un individu
     *
     * @param ind L'individu a deplacer
     * @param group Groupe auquel appartient l'individu
     * @param dest Cellule de destination de l'individu
     * @return /
     */
    public void moveAquaNism(Individual ind, AquaNismsGroup<?, ?> group, Cell dest) {
        super.moveAquaNism(ind, group, dest);
        this.removeAquaNism(ind, group);
        dest.addPike((Pike) ind);
    }

    /**
     * Procedure qui enleve un individu
     *
     * @param ind L'individu a enlever
     * @param group Groupe auquel appartient l'individu
     * @return /
     */
    public void removeAquaNism(Individual ind, AquaNismsGroup<?, ?> group) {
        super.removeAquaNism(ind, group);
        ind.getPosition().removePike((Pike) ind);
    }

    /**
     * Calcul des cellules accessibles (comprise dans le lac)
     *
     * @param position ID de la cellule initiale
     * @return neighbours Liste des cellules (id) accessibles
     */
    public List<Cell> getNeighbours(Cell position) {

        List<Cell> neighbours = new ArrayList<Cell>();
        int[][] listeCoord;
        int xPike = 0, yPike = 0;

        // Calcul des coordonnees (x, y) a partir de l'Id de la maille
        yPike = (position.getIndex() / gridWidth);
        xPike = (position.getIndex() - (yPike * gridWidth));

        // Determination de la liste des mailles pour une distance donnee qui depend de la phase du jour et de la saison
        switch (Time.getSeason()) {
            case "PRINTEMPS":
                switch (Time.getPhaseJour()) {
                    case "AUBE":
                        listeCoord = AreaMovement.areaPrinAube;
                        break;
                    case "JOUR":
                        listeCoord = AreaMovement.areaPrinJour;
                        break;
                    case "NUIT":
                        listeCoord = AreaMovement.areaPrinNuit;
                        break;
                    case "CREP":
                        listeCoord = AreaMovement.areaPrinCrep;
                        break;
                    default:
                        System.out.println("Erreur lecture de la Phase du Jour au Printemps");
                        listeCoord = AreaMovement.areaPrinJour;
                        break;
                }
                break;

            case "ETE":
                switch (Time.getPhaseJour()) {
                    case "AUBE":
                        listeCoord = AreaMovement.areaEteAube;
                        break;
                    case "JOUR":
                        listeCoord = AreaMovement.areaEteJour;
                        break;
                    case "NUIT":
                        listeCoord = AreaMovement.areaEteNuit;
                        break;
                    case "CREP":
                        listeCoord = AreaMovement.areaEteCrep;
                        break;
                    default:
                        System.out.println("Erreur lecture de la Phase du Jour en Ete");
                        listeCoord = AreaMovement.areaEteJour;
                        break;
                }
                break;

            case "AUTOMNE":
                switch (Time.getPhaseJour()) {
                    case "AUBE":
                        listeCoord = AreaMovement.areaAutAube;
                        break;
                    case "JOUR":
                        listeCoord = AreaMovement.areaAutJour;
                        break;
                    case "NUIT":
                        listeCoord = AreaMovement.areaAutNuit;
                        break;
                    case "CREP":
                        listeCoord = AreaMovement.areaAutCrep;
                        break;
                    default:
                        System.out.println("Erreur lecture de la Phase du Jour en Automne");
                        listeCoord = AreaMovement.areaAutJour;
                        break;
                }
                break;

            case "HIVER":
                switch (Time.getPhaseJour()) {
                    case "AUBE":
                        listeCoord = AreaMovement.areaHivAube;
                        break;
                    case "JOUR":
                        listeCoord = AreaMovement.areaHivJour;
                        break;
                    case "NUIT":
                        listeCoord = AreaMovement.areaHivNuit;
                        break;
                    case "CREP":
                        listeCoord = AreaMovement.areaHivCrep;
                        break;
                    default:
                        System.out.println("Erreur lecture de la Phase du Jour en Hiver");
                        listeCoord = AreaMovement.areaHivJour;
                        break;
                }
                break;

            default:
                System.out.println("Erreur lecture de la Saison");
                switch (Time.getPhaseJour()) {
                    case "AUBE":
                        listeCoord = AreaMovement.areaPrinAube;
                        break;
                    case "JOUR":
                        listeCoord = AreaMovement.areaPrinJour;
                        break;
                    case "NUIT":
                        listeCoord = AreaMovement.areaPrinNuit;
                        break;
                    case "CREP":
                        listeCoord = AreaMovement.areaPrinCrep;
                        break;
                    default:
                        System.out.println("Plus : Erreur lecture de la Phase du Jour");
                        listeCoord = AreaMovement.areaPrinJour;
                        break;
                }
                break;
        }

        // Calcul des mailles comprises dans le lac
        boolean stop = false;
        Cell cellule = null;
        for (int cpt = 0; cpt < listeCoord[0].length; cpt++) {

            if (listeCoord[0][cpt] == 0 && listeCoord[1][cpt] == 0 && stop == true) {
                break;
            }
            if (listeCoord[0][cpt] == 0 && listeCoord[1][cpt] == 0) {
                stop = true;
            }

            // Calcul de l'indice de la maille � partir des coordonnees (x,y)
            int newCell = xPike + listeCoord[0][cpt] + (yPike + listeCoord[1][cpt]) * gridWidth;

            // Test si l'indice calcule appartient � la grille totale
            if (newCell >= 0 && newCell <= cellnumber_2) {
                cellule = getCell(newCell);
                // Si l'indice calcule est compris dans le lac on l'ajoute a la liste
                if (cellule.getHabitatQuality() >= 0) {
                    neighbours.add(cellule);
                }
            }
        }
        return neighbours;
    }

    private int computeIndex(int xPike, int[][] listeCoord, int cpt, int yPike) {
        return xPike + listeCoord[0][cpt] + (yPike + listeCoord[1][cpt]) * gridWidth;
    }
}
