package pikelake.environment;

import pikelake.pikes.Pike;
import pikelake.pikes.PikesGroup;
import pikelake.environment.HoraireLeverCoucher;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 *		Classe de la gestion du temps de la simulation
 *		@author Guillaume GARBAY
 *		@version 1.0
 */
public class Time extends AquaNismsGroupProcess<Pike,PikesGroup> {

		// Variables representant la date de la simulation
		public static enum Season {PRINTEMPS, ETE, AUTOMNE, HIVER};
		public static enum Mois {MoisZero, JANVIER, FEVRIER, MARS, AVRIL, MAI, JUIN, JUILLET, AOUT, SEPTEMBRE, OCTOBRE, NOVEMBRE, DECEMBRE};
		public static int mois = 1, jour = 0, jourMois = 1, saison = 0, annee = 2012, phaseJour = 0, heure = 0;
		public static String PhaseJour;
		public static long nbrIter = 0;
		
		/**
		 *		Determine la date de la simulation en fonction du nombre de pas de la simulation
		 *		Calcule l'heure, le jour, la phase du jour, le mois, la saison et l'ann�e
		 *		!! 1 <= jour <= 365	;	1 <= jourMois <= 30,31 !!
		 *		@param group L'ensemble des individus
		 *		@return /
		 */
		@Override
		public void doProcess (PikesGroup group) {
			
			// Nombre de pas de la simulation
			nbrIter = group.getPilot().getCurrentTime();
			
			// Pas de temps Horaire
			heure = (int) nbrIter % 24;	
			jour = (int) Math.ceil((nbrIter+1) / 24.);
			
			// RAZ du nombre de jour par an si l'annee est finie
			if (jour>365) {
				annee = annee + 1;
				jour = jour - 365;
			}
			
			// D�termination du jour dans le mois
			if (heure == 0)
				jourMois = jourMois + 1;
			
			// RAZ jourMois
			if ((mois == 1 | mois == 3 | mois == 5 | mois == 7 | mois == 8 | mois == 10 | mois == 12) & jourMois == 32)	jourMois = 1;
			else if ((mois == 4 | mois == 6 | mois == 9 | mois == 11) & jourMois == 31)									jourMois = 1;
			else if  (mois == 2 & jourMois == 29)																		jourMois = 1;
			
			// Calcul du mois, de la saison et de la phase du jour
			calculMois();
			calculSaison();
			PhaseJour = HoraireLeverCoucher.phaseJournee[jourMois][mois][heure];
			
			// Affichage de la date complete a chaque pas de temps
			//System.out.println(annee + "	" + Season.values()[saison] + "	" + Mois.values()[mois] + "	" + jour + "	" + jourMois + "	" + heure + "	" + PhaseJour);
		}

		/**
		 *		D�termine le mois en fonction du jour par an
		 *		@param / 
		 *		@return /
		 */
		public void calculMois () {
			if		(jour <= 31)	mois = 1; // Janv
			else if (jour <= 59)	mois = 2; // Fev
			else if (jour <= 90)	mois = 3; // Mars
			else if (jour <= 120)	mois = 4;// Avril
			else if (jour <= 151)	mois = 5; // Mai
			else if (jour <= 181)	mois = 6; // Juin
			else if (jour <= 212)	mois = 7; // Juillet
			else if (jour <= 243)	mois = 8; // Aout
			else if (jour <= 273)	mois = 9; // Sept
			else if (jour <= 304)	mois = 10; // Oct
			else if (jour <= 334)	mois = 11; // Nov
			else if (jour <= 365)	mois = 12; // Dec
			else					mois = 0;
		}
		
		/**
		 *		D�termine la saison en fonction du mois et du jour du mois
		 *		@param / 
		 *		@return /
		 */
		public void calculSaison () {
			if 		(mois == 1)						saison = 3;
			if 		(mois == 3 & jourMois >= 20)	saison = 0;
			else if (mois == 6 & jourMois >= 20)	saison = 1;
			else if (mois == 9 & jourMois >= 22)	saison = 2;
			else if (mois == 12 & jourMois >= 21)	saison = 3;
		}
		
		/**
		 *		Retourne la phase du jour 
		 *		@param / 
		 *		@return PhaseJour Chaine de caract�re contenant la phase du jour
		 */
		public static String getPhaseJour () {					
			return PhaseJour;
		}
		
		/**
		 *		Retourne le mois
		 *		@param / 
		 *		@return Mois[i] Chaine de caract�re contenant le mois
		 */
		public static String getMois () {					
			return Mois.values()[mois].toString();
		}

		/**
		 *		Retourne la saison
		 *		@param / 
		 *		@return Season[i] Chaine de caract�re contenant la saison
		 */
		public static String getSeason () {
			return Season.values()[saison].toString();
		}
}
