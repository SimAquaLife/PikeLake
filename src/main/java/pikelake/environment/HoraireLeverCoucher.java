package pikelake.environment;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

import pikelake.pikes.Pike;
import pikelake.pikes.PikesGroup;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 *		Classe qui lit le fichier contenant les horaires de lever et de coucher du soleil
 *		Ces horaires sont rang�s a l'initialisation dans un tableau pour un acces plus rapide
 *		@author Guillaume GARBAY
 *		@version 1.0
 */
public class HoraireLeverCoucher extends AquaNismsGroupProcess<Pike,PikesGroup> {

	// Tableaux contenant les horaires de lever et coucher du soleil ainsi que les phases du jour en fonction de la date
	public static int heureLeverCoucher[][][] = new int[2][32][13] ;	//	[0=lever, 1=coucher]	[jour]	[mois]	
	public static String phaseJournee[][][] = new String[32][13][25];	//	[jour]	[mois]	[heure]

	/**
	 *		Lis le fichier contenant les horaires de lever et de coucher du soleil
	 *		Remplis les tableaux heureLeverCoucher et phaseJournee
	 *		@param object L'ensemble des individus
	 *		@return /
	 */
	@Override
	public void doProcess (PikesGroup object) {
		
		// Variables qui recuperent les valeurs lues dans le fichier
		String temp[] = null, dateCompleteLever[] = null, dateCompleteCoucher[] = null;
		String jour = null, mois = null, heureLever = null, heureCoucher = null;
	
		// Fichier contenant les horaires de lever et de coucher du soleil
		String filePath = "data/input/leshorairesdusoleil.csv";    	
		Scanner scanner;
		try {
			scanner = new Scanner(new File(filePath));
			String line = scanner.nextLine();
			
			// On boucle sur chaque ligne detect�e
			while (scanner.hasNextLine()) {
				
				line = scanner.nextLine();	// Lecture de la ligne
				temp = line.split(";");		// Decoupage de la ligne

				// D�termination heure de lever du soleil
				dateCompleteLever = temp[0].split("[/: ]+");
				dateCompleteCoucher = temp[1].split("[/: ]+");
					// Calcul date
				jour = dateCompleteLever[0];
				mois = dateCompleteLever[1];
					// Lever du soleil
				heureLever = dateCompleteLever[3];
					// Coucher du soleil
				heureCoucher = dateCompleteCoucher[3];
				
				
				heureLeverCoucher[0][Integer.parseInt(jour)][Integer.parseInt(mois)] = Integer.parseInt(heureLever);
				heureLeverCoucher[1][Integer.parseInt(jour)][Integer.parseInt(mois)] = Integer.parseInt(heureCoucher);				
			}
			scanner.close(); // Fermeture du fichier
		} catch (FileNotFoundException e) {
			// Auto-generated catch block, erreur de lecture du fichier
			e.printStackTrace();
		}
		
		// Remplissage du tableau qui determine la phase du jour avec les horaires lus dans le fichier
		for (int m=1; m<13 ;m++)
			for (int j=1; j<32; j++) 
				for (int h=0; h<25; h++) {
					if		(h > heureLeverCoucher[0][j][m] -2 & h < heureLeverCoucher[0][j][m] +2)	phaseJournee[j][m][h] = "AUBE";
					else if (h > heureLeverCoucher[0][j][m] +1 & h < heureLeverCoucher[1][j][m] -1)	phaseJournee[j][m][h] = "JOUR";
					else if (h > heureLeverCoucher[1][j][m] -2 & h < heureLeverCoucher[1][j][m] +2)	phaseJournee[j][m][h] = "CREP";
					else																			phaseJournee[j][m][h] = "NUIT";
				}			
	}	
	
	/**
	 *		Retourne la phase du jour en fonction d'une date (jour+mois+heure)
	 *		@param jour Entier contenant le jour
	 *		@param mois Entier contenant le mois
	 *		@param heure Entier contenant l'heure
	 *		@return phaseJournee[jour][mois][heure] Chaine de caract�re contenant la phase du jour
	 */
	public static String getPhase (int jour, int mois, int heure) {
		return phaseJournee[jour][mois][heure];
	}
	
	/**
	 *		Retourne l'horaire de lever ou de coucher en fonction d'une date (jour+mois)
	 *		@param levercoucher Entier ou booleen 0=lever, 1=coucher		 
	 *		@param jour Entier contenant le jour
	 *		@param mois Entier contenant le mois
	 *		@return heureLeverCoucher[0=lever, 1=coucher][jour][mois] Entier representant l'heure du lever ou du coucher
	 */
	public int getHoraire (int levercoucher, int jour, int mois) {
		return heureLeverCoucher[levercoucher][jour][mois] ;
	}
	
}
