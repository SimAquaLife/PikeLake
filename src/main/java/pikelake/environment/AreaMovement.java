package pikelake.environment;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import pikelake.pikes.Pike;
import pikelake.pikes.PikesGroup;

/**
 *		Classe qui lit le fichier contenant les distances horaires parcourues par les individus,
 *		selon la saison et la phase du jour.
 *		Trois distances sont disponibles (min, moy, max), choix de la distance utilisee dans le fichier fishLight.xml
 *		@author Guillaume GARBAY
 *		@version 1.0
 */
public class AreaMovement extends AquaNismsGroupProcess<Pike, PikesGroup> {

	private int distMin = 0, distMoy = 0, distMax = 0, std = 0, choixDist = 0;
	private String saison = null, phaseJour = null;

	// Tableaux d'entier contenant les coordonnees des cellules accessibles
	public static int[][] areaPrinAube, areaPrinJour, areaPrinNuit, areaPrinCrep;
	public static int[][] areaEteAube,  areaEteJour,  areaEteNuit,  areaEteCrep;
	public static int[][] areaAutAube,  areaAutJour,  areaAutNuit,  areaAutCrep;
	public static int[][] areaHivAube,  areaHivJour,  areaHivNuit,  areaHivCrep;
	
	/**
	 *		Lis le fichier contenant les distances horaires en fonction de la saison et de la phase du jour.
	 *		Remplis les tableaux contenant les coordonnees des cellules accessibles
	 *		@param object L'ensemble des individus 
	 *		@return / 
	 */
	@Override
	public void doProcess (PikesGroup object) {
		// Lecture fichier contenant les distances Horaires Cartesienne
		String filePath = "data/input/DistanceHoraireCartesienne_Bro.txt";    	
		Scanner scanner;
		try {
			scanner = new Scanner(new File(filePath));
			String line = scanner.nextLine();
			
			// On boucle sur chaque ligne detect�e
			while (scanner.hasNextLine()) {
				line = scanner.nextLine();		
				
				// Decoupage ligne : saison ; phase du jour ; distance : minimun, moyenne, maximum et std
				StringTokenizer sLigne = new StringTokenizer (line);
				if (sLigne.hasMoreTokens())
					saison = sLigne.nextToken();
				if (sLigne.hasMoreTokens())
					phaseJour = sLigne.nextToken();
				if (sLigne.hasMoreTokens())
					distMin = Integer.parseInt(sLigne.nextToken());
				if (sLigne.hasMoreTokens())
					distMoy = Integer.parseInt(sLigne.nextToken());
				if (sLigne.hasMoreTokens())
					distMax = Integer.parseInt(sLigne.nextToken());
				if (sLigne.hasMoreTokens())
					std = Integer.parseInt(sLigne.nextToken());

				// Remplissage du tableau correspondant a la ligne lue
				if (saison.equals("PRINTEMPS")) {
					if 		(phaseJour.equals("AUBE"))	areaPrinAube = choixDistArea ();
					else if (phaseJour.equals("JOUR"))	areaPrinJour = choixDistArea ();
					else if (phaseJour.equals("CREP"))	areaPrinCrep = choixDistArea ();
					else if (phaseJour.equals("NUIT"))	areaPrinNuit = choixDistArea ();
					
				} else if (saison.equals("ETE")) {
					if		(phaseJour.equals("AUBE"))	areaEteAube = choixDistArea ();
					else if (phaseJour.equals("JOUR"))	areaEteJour = choixDistArea ();
					else if (phaseJour.equals("CREP"))	areaEteCrep = choixDistArea ();
					else if (phaseJour.equals("NUIT"))	areaEteNuit = choixDistArea ();
			
				} else if (saison.equals("AUTOMNE")) {
					if		(phaseJour.equals("AUBE"))	areaAutAube = choixDistArea ();
					else if (phaseJour.equals("JOUR"))	areaAutJour = choixDistArea ();
					else if (phaseJour.equals("CREP"))	areaAutCrep = choixDistArea ();
					else if (phaseJour.equals("NUIT"))	areaAutNuit = choixDistArea ();
				
				} else if (saison.equals("HIVER")) {
					if		(phaseJour.equals("AUBE"))	areaHivAube = choixDistArea ();
					else if (phaseJour.equals("JOUR"))	areaHivJour = choixDistArea ();
					else if (phaseJour.equals("CREP"))	areaHivCrep = choixDistArea ();
					else if (phaseJour.equals("NUIT"))	areaHivNuit = choixDistArea ();
				}		
			}
		scanner.close(); // Fermeture du fichier
		} catch (FileNotFoundException e) {
			// Auto-generated catch block, erreur de lecture du fichier
			e.printStackTrace();
		}
	}
	
	/**
	 *		Determine la distance qui sera utilisee pour calculer les aires de deplacement
	 *		@param / 
	 *		@return temp Tableau d'entier contenant les coordonnees des cellules accessibles
	 */
	public int[][] choixDistArea () {
		int[][] temp = null;
		if 		(choixDist == 0) 	temp = calculationArea (distMin);
		else if (choixDist == 1)	temp = calculationArea (distMoy);
		else if (choixDist == 2)	temp = calculationArea (distMax);
                return temp;
	}
	
    /**
     * Determine les coordonnees du cercle des cellules accessibles pour une
     * distance donnee
     *
     * @param distance Entier representant la distance de deplacement
     * @return area Tableau d'entier contenant les coordonnees des cellules
     * accessibles
     */
    public int[][] calculationArea(int distance) {

        List<List<Integer>> area = new ArrayList<List<Integer>>();
        area.add(new ArrayList<Integer>());
        area.add(new ArrayList<Integer>());
        int xi = 0, yi = 0, cpt = 0;

        // Conversion distance en nombre de cellule
        int distCell = (int) Math.round(distance / 10.);

        // Calcul des coordonnees (x,y) des cellules pour une distance donnee		
        for (xi = -distCell; xi <= distCell; xi++) {
            yi = (int) Math.round(Math.sqrt(distCell * distCell - xi * xi));
            for (int j = -yi; j <= yi; j++) {
                area.get(0).add(xi);
                area.get(1).add(j);
            }
        }
        int[][] result = new int[2][area.get(0).size()];
        for (int i = 0; i < area.size(); i++) {
            for (int j = 0; j < area.get(i).size(); j++) {
                result[i][j] = area.get(i).get(j);
            }
        }

        return result;
    }
	
	/**
	 *		Retourne la distance minimum
	 *		@param / 
	 *		@return distMin Entier contenant la distance minimum
	 */
	public int getDistMin () {
		return distMin;
	}
	
	/**
	 *		Retourne la distance moyenne
	 *		@param / 
	 *		@return distMoy Entier contenant la distance moyenne
	 */
	public int getDistMoy () {
		return distMoy;
	}
	
	/**
	 *		Retourne la distance maximum
	 *		@param / 
	 *		@return distMax Entier contenant la distance maximum
	 */
	public int getDistMax () {
		return distMax;
	}
	
	/**
	 *		Retourne l'ecart type de la distance (std)
	 *		@param / 
	 *		@return std Entier contenant la distance std
	 */
	public int getDistStd () {
		return std;
	}

	/**
	 *		Retourne la saison
	 *		@param / 
	 *		@return saison Chaine de caract�re contenant la saison
	 */
	public String getSaison () {
		return saison;
	}
	
	/**
	 *		Retourne la phase du jour
	 *		@param / 
	 *		@return phaseJour Chaine de caract�re contenant la phase du jour
	 */
	public String getPhaseJour () {
		return phaseJour;
	}

}
