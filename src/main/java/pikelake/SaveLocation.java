package pikelake;

import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;

import pikelake.pikes.Pike;
import pikelake.pikes.PikesGroup;
import pikelake.pikes.PikeTrackLocation;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 *		Classe qui sauvegarde la position des individus a la fin de la simulation
 *		Les positions ainsi que la date correspondant a chacune est enregistre dans un fichier de sortie
 *		
 *		Extrait du fichier de sortie :
 *		PasTps 		 Date 		Ind 1 	Ind 2 	Ind 3 	Ind 4 	Ind 5 	Ind 6 	Ind 7 	...
 *		1		2012/01/01/01	4530	5391	5606	5392	52066	52282	51444	...
 *		
 *		@author Guillaume GARBAY
 *		@version 1.0
 */
public class SaveLocation extends AquaNismsGroupProcess<Pike, PikesGroup> {

	/**
	 *		Procedure qui, a la fin de la simulation, sauvegarde la position des individus dans un fichier de sortie
	 *		@param group Represente les individus
	 *		@return /
	 */
	@Override
	public void doProcess (PikesGroup group) {
		
		String ligne = null;
		DecimalFormat df = new DecimalFormat("00");
		
		// Declaration du fichier de sortie
		final String chemin = "data/output/Positions.txt";
		final File fichier = new File(chemin);
		
		try {
			// Creation du fichier
			fichier.createNewFile();
			
			// Creation d'un writer (un �crivain)
			final FileWriter writer = new FileWriter(fichier);
			try {
				// Creation de la ligne d'en-tete du fichier
				ligne = "PasTps \t\t Date \t\t";
				for (int i=1; i!=Pike.cptIndividu; i++) {
					ligne = ligne + "Ind " + i + " \t";
				}
				writer.write(ligne + "\n\r");
				
				// Ecriture de chaque ligne de donnees (date + positions)
				for (int compteur = 0; compteur < PikeTrackLocation.trackLocation[1].length; compteur++) {
					
					// Ecriture pas de la simulation + de la date			 	
					ligne = String.valueOf(PikeTrackLocation.trackDate[0][compteur]) + "\t\t";
					ligne = ligne + PikeTrackLocation.trackDate[1][compteur] + "/" +
									df.format(PikeTrackLocation.trackDate[2][compteur]) + "/" + 
									df.format(PikeTrackLocation.trackDate[3][compteur]) + "/" + 
									df.format(PikeTrackLocation.trackDate[4][compteur]) + "\t";
					
					// Ecriture des positions
					for (int i=1; i<PikeTrackLocation.trackLocation.length; i++) {
						ligne = ligne + PikeTrackLocation.trackLocation[i][compteur] + "\t";;
					}
					ligne = ligne + "\n";
					writer.write(ligne);
				}	
			} finally {
				writer.close(); // Fermeture du fichier
			}
		} catch (Exception e) { // Erreur de creation du fichier
			System.out.println("Impossible de creer le fichier d'enregistrement des positions.");
		}
	}	
}
