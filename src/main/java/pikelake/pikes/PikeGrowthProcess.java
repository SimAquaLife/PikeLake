package pikelake.pikes;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

public class PikeGrowthProcess extends AquaNismsGroupProcess<Pike,PikesGroup> {

	/**
	 * <code>convertionFactor</code> proportion of the ingested food transformed into pike weight 
	 */
	private double convertionFactor = 0.25;
	private double slimRate = 0.90;
	
	/* (non-Javadoc)
	 * @see fr.cemagref.simaqualife.kernel.processes.Process#doProcess(java.lang.Object)
	 */
	@Override
	public void doProcess (PikesGroup group) {
		System.out.println("PikeGrowthProcess");
		
		for (Pike pike : group.getAquaNismsList()){
			pike.incAge();
			//System.out.print("  "+ (double) pike.getAge()/12. +"y "+pike.getWeight()+ " " );
			pike.setWeight( pike.getWeight() * slimRate + pike.getIngestedFood()* convertionFactor);
			//System.out.print(pike.getIngestedFood() +" " + pike.getWeight());
			//double ratio = 12* pike.getWeight()/(pike.getAge()+12);
			//System.out.println("("+ ((double) Math.round(ratio*100))/100 +")");
			pike.setIngestedFood(0.0);
		}
	}

}


