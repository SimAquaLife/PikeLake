package pikelake.pikes;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

public class PikePlopProcess extends AquaNismsGroupProcess<Pike,PikesGroup> {

	private int temporisation = 3000; // in ms
	
	public void doProcess (PikesGroup object) {
		System.out.println("PikePlopProcess");
		
    	try {
			Thread.sleep(temporisation);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
