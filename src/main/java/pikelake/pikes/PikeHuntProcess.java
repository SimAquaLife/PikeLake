package pikelake.pikes;

//import java.util.Collections;
import java.util.Comparator;
//import java.util.List;




//import pikelake.Cell;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author patrick.lambert
 * 
 */
public class PikeHuntProcess extends
LoopAquaNismsGroupProcess<Pike, AquaNismsGroup<Pike, ?>> {

	/**
	 * <code>satiety</code> ratio of the pike weigth which stops eating
	 * preys
	 */
	@SuppressWarnings("unused")
	private double satiety = 0.10;

	@SuppressWarnings("unused")
	transient private UniformGen uniformGen;

	@Override
	protected void doProcess (Pike pike, AquaNismsGroup<Pike, ?> group) {
		
		System.out.println("PikeHuntProcess");
		
//		Cell pikeCell = pike.getPosition();
/*		if (pikeCell.getPreys().size() > 0) {
			//System.out.print(" " +  pikeCell.getPreys().size() +" preys available in cell ");
			//System.out.print("("+pikeCell.getX()+","+pikeCell.getY()+")");
			
			// sort the preys list according to the weigth
			List<Prey> sortedPreys = pikeCell.getPreys();
			Collections.sort(sortedPreys);

			double ratio = pike.getIngestedFood()/pike.getWeight();
			int idx = 0;
			int idxMax =  sortedPreys.size();
			int target=0;
			int eaten=0;
			while ((pike.getIngestedFood()/pike.getWeight() < satiety) && (idx < idxMax)) {

				// the probability for a prey to be eaten depends 
				// on the habitat quality in the cell
				if (uniformGen.nextDouble() < pikeCell.getHabitatQuality()){
					// eat the prey
					Prey eatenPrey = sortedPreys.get(target);
					pike.addIngestedFood(eatenPrey.getWeight());

					// and the prey dies
					
					((PreysGroup)group.getPilot().getAquaticWorld().getAquaNismsGroupsList().get(0)).removeAquaNism(eatenPrey);
					//this.interAquanism(eatenPrey);
					pikeCell.removePrey(eatenPrey);
					eaten ++;
				}
				else
					target ++;

				idx++;
				ratio = pike.getIngestedFood()/pike.getWeight();
			}
			ratio= ((double) Math.round(100*ratio))/100;
			//System.out.println(", " + eaten + " preys eaten ("+ ratio +" ), still " +  pikeCell.getPreys().size());
		}	*/

	}

	@InitTransientParameters
	public void initTransientParameters (Pilot pilot) {
		sorted = true;
		comparator = new Comparator<Pike>() {
			public int compare(Pike o1, Pike o2) {
				return (int) Math.signum(o2.getWeight() - o1.getWeight());
			}
		};

		uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist());
	}

}
