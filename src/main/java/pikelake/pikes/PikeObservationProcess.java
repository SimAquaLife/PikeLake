package pikelake.pikes;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

public class PikeObservationProcess extends AquaNismsGroupProcess<Pike,PikesGroup> {

	public void doProcess (PikesGroup pikesGroup) {
		System.out.println("PikeObservationProcess");
		
		pikesGroup.calculatePikesBiomass();
		pikesGroup.calculatePikesNumber();
		
	}
}
