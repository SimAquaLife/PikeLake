package pikelake.pikes;

import pikelake.Grid;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.Processes;
import fr.cemagref.simaqualife.pilot.Pilot;

public class PikesGroup extends AquaNismsGroup<Pike,Grid> {
	
	/**
	 * <code>weightAtAgeThreshold</code> threshold of the ratio of  weight (in g) by age (in year)
	 */
	private double weightAtAgeThreshold = 5;
	
	
	/**
	 * <code>monthOfBirth</code> month of reproduction
	 */
	private int monthOfBirth = 6;
	
	
	@Observable(description = "Pikes biomass (g)")
	private transient double pikesBiomass;
	
	@Observable(description = "Pikes number")
	private transient int pikesNumber;
	
	
	public PikesGroup (Pilot pilot, Grid arg0, Processes arg1) {
		super(pilot, arg0, arg1);
	}
	
	public int calculatePikesNumber () {
		pikesNumber = this.getAquaNismsList().size();
		
		return pikesNumber;
	}

	public double calculatePikesBiomass () {
		pikesBiomass =0.;
			for(Pike pike : this.getAquaNismsList())
				pikesBiomass += pike.getWeight();
		return pikesBiomass;
	}


	public int getMonthOfBirth () {
		return monthOfBirth;
	}

	public double getWeightAtAgeThreshold () {
		return weightAtAgeThreshold;
	}
}
