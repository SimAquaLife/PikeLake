package pikelake.pikes;

import fr.cemagref.simaqualife.kernel.processes.LoopAquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;

import java.util.ArrayList;
import java.util.List;

import pikelake.Cell;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.UniformGen;
import fr.cemagref.simaqualife.pilot.Pilot;

public class PikeMovement extends LoopAquaNismsGroupProcess<Pike, PikesGroup> {

	//@SuppressWarnings("unused")
	//private double basicMovingRate;
    transient private UniformGen uniformGen;

    public PikeMovement (Pilot pilot) {
        uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist());
    }

    @InitTransientParameters
    public void initTransientParameters (Pilot pilot) {
        uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist(0, 1));
    }

    @Override
    protected void doProcess (Pike pike, PikesGroup group) {
    	
    	
    	// Recupération HSI de la cellule occupee
        double cellSuitability = pike.getSuitabilityForPike(pike.getPosition());

        // Calcul de la liste des cellules accessibles (distance donnee par fichier entree: DistHorCartBro)
        final List<Cell> surrounding = group.getEnvironment().getNeighbours(pike.getPosition());  
   

        // the first possiblity is the cell where the prey is
        List<Cell> possibilities = new ArrayList<Cell>();
        possibilities.add(pike.getPosition());
        
        // identify the destination possibilities in the neighbouring
        // with the highest suitability
        for (Cell cell : surrounding) {
        	
            double currentCellSuitability = pike.getSuitabilityForPike(cell);
                       
            if (currentCellSuitability >= 0) {
            	if (currentCellSuitability > cellSuitability) {
            		cellSuitability = currentCellSuitability;
            		possibilities.clear();
            		possibilities.add(cell);
            	} else if (currentCellSuitability == cellSuitability) {
            		possibilities.add(cell);
            	}
            }
        }

        // choose the destination cell
        int possibilitiesNumber = possibilities.size();
        if (possibilitiesNumber == 1) {
            pike.moveTo(group.getPilot(), possibilities.get(0), group);
        } else {
            int idx = (int) Math.floor(uniformGen.nextDouble() * (double) possibilitiesNumber);
            pike.moveTo(group.getPilot(), possibilities.get(idx), group);
        }
    }
}