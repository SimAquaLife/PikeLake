package pikelake.pikes;

import pikelake.Cell;

import umontreal.iro.lecuyer.randvar.UniformGen;
import umontreal.iro.lecuyer.probdist.UniformDist;

import fr.cemagref.simaqualife.pilot.Pilot;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;

/**
 *		Classes qui initialise l'ensemble de la population
 *		D�finit aleatoirement la position initiale de chaque individu
 *		@author Guillaume GARBAY
 *		@version 1.0
 */
public class PikesPopulateProcess extends AquaNismsGroupProcess<Pike,PikesGroup> {

	private int initialNumberOfPikes =5 ;	
	transient private UniformGen uniformGen;
	
	/**
	 *		Procedure d'initialisation de la variable uniformGen
	 *		@param pilot
	 *		@return /
	 */
	@InitTransientParameters
	public void initTransientParameters (Pilot pilot) {
		double nbCell = (double) this.getGroup().getEnvironment().getCells().length;
		uniformGen = new UniformGen(pilot.getRandomStream(), new UniformDist(0,nbCell-1));
	}

	/**
	 *		Procedure qui place chaque individu dans le lac
	 *		Appelee lors de l'initialisation du programme
	 *		@param group Represente les individus
	 *		@return /
	 */
	public void doProcess (PikesGroup group) {
		
		int indexCell;
		Cell[] cells = group.getEnvironment().getCells();
		
		for (int i=0; i < initialNumberOfPikes ;i++){
			
			// Declaration de l'age et du poids initiaux pour chaque individus
			int ageAtCreation = (int)(12+group.getPilot().getCurrentTime()-group.getMonthOfBirth())%12 +12*(i%5);
			double weightAtCreation = 2*group.getWeightAtAgeThreshold() * (ageAtCreation+12) / 12;
			
			// Place l'individu dans une cellule avec un HSI > 0, cad � l'interieur du lac
			do { indexCell = (int) Math.round(uniformGen.nextDouble());				
			} while (cells[indexCell].getHabitatQuality() < 0);
			
			// Creation du nouvel individu
			Pike newPike = new Pike(group.getPilot(), cells[indexCell], ageAtCreation, weightAtCreation);
    		group.addAquaNism(newPike);
		}		
	}
}
