package pikelake.pikes;

import pikelake.environment.Time;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 *		Classe qui enregistre la position des individus a chaque pas de temps
 *		Les positions ainsi que la date correspondant a chacune est enregistre dans des tableaux
 *		@author Guillaume GARBAY
 *		@version 1.0
 */
public class PikeTrackLocation extends AquaNismsGroupProcess<Pike, PikesGroup> {

	// Tableaux d'entiers comprenant les positions de chaque individu et la date correspondante
	public static int trackLocation[][] = new int [50][8760];
	public static int trackDate[][] = new int[5][8760];
	public static int cpt = 0;
	
	/**
	 *		Procedure qui, a chaque pas de temps, enregistre la date courante, le nombre de pas dans le tableau trackDate
	 *		Les positions de chaque individus sont enregistres dans le tableau  trackLocation
	 *		@param group Represente les individus
	 *		@return /
	 */		
	public void doProcess (PikesGroup group) {
		
		// Enregistrement de la date + nombre de pas de la simulation
		trackDate[0][cpt] = (int) group.getPilot().getCurrentTime();	// pas de temps de la simulation
		trackDate[1][cpt] = Time.annee;
		trackDate[2][cpt] = Time.mois;
		trackDate[3][cpt] = Time.jourMois;
		trackDate[4][cpt] = Time.heure;
		
		// Enregistrement de la position des individus
		for (Pike pike : group.getAquaNismsList())
			trackLocation[pike.idIndividu][cpt] = pike.getPosition().getIndex();
		
		cpt++;
	}
}
