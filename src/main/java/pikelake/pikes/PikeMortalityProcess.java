package pikelake.pikes;

import java.util.ArrayList;
import java.util.List;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;


public class PikeMortalityProcess extends AquaNismsGroupProcess<Pike, PikesGroup>{

	public void doProcess (PikesGroup group) {
		System.out.println("PikeMortalityProcess");
		
		List<Pike> deadPike = new ArrayList<Pike>();
		for (Pike pike: group.getAquaNismsList()){
			double ratio = (12. * pike.getWeight() / (double) (pike.getAge()+12));

			if (ratio < group.getWeightAtAgeThreshold())
				deadPike.add(pike);
		}
	    // update the pikesGroup
		for (Pike pike: deadPike){
			group.getAquaNismsList().remove(pike);
			//ASK WHY
			pike.getPosition().removePike(pike);
		}
	}
}
