package pikelake.pikes;

import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.simaqualife.pilot.Pilot;
import pikelake.Cell;
import pikelake.Individual;

public class Pike extends Individual {

	private double ingestedFood=0.;
	
	public static int cptIndividu = 1;
	public int idIndividu = 0;
	
    public Pike (Pilot pilot, Cell cell) {
    	//TODO fix weight according to the weightAtAgeThreshold
        // Default value of weight at birth
    	this(pilot, cell, 0, 10.);
    	idIndividu = cptIndividu ++;
    }
    
    public Pike (Pilot pilot, Cell cell, int age, double weight) {
        super(pilot, cell);
        this.age =age;
        this.weight = weight;
        idIndividu = cptIndividu ++;
    }
    
    @Observable(description="pike id")
    public final int getIdIndividu() {
        return idIndividu;
    }

    @Observable(description="cell index")
    public int getCellIndex() {
        return getPosition().getIndex();
    }
    
    @Observable(description="cell habitat quality")
    public double getCellHabitatQuality() {
        return getPosition().getHabitatQuality();
    }
	public double getIngestedFood() {
		return ingestedFood;
	}

	public void setIngestedFood (double ingestedFood) {
		this.ingestedFood = ingestedFood;
	}
	public void addIngestedFood (double ingestedFood) {
		this.ingestedFood += ingestedFood;
	}
	
	public double getSuitabilityForPike (Cell cell){
		if (cell.getPikes().size()>1)
			return -1.; // at least an other pike in the cell
		else
		{
			//System.out.println(cell.getIndex());
			//System.out.println(cell.getHabitatQuality());
			return cell.getHabitatQuality();
		}
			//return((double) cell.getPreys().size()) * cell.getHabitatQuality(); // number of preys accessible
	}
	
	
}

