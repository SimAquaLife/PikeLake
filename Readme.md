# PikeLake

This model uses the framework [SimAquaLife](https://gitlab.irstea.fr/SimAquaLife/SimAquaLife).


```
# package the project in a single file
mvn assembly:assembly -DdescriptorId=jar-with-dependencies
# run a simulation
java -cp target/PikeLake-1.0-SNAPSHOT-jar-with-dependencies.jar fr.cemagref.simaqualife.extensions.pilot.BatchRunner -simDuration 8760 -groups data/input/fishLight.xml -env data/input/grid.xml -observers data/input/observersBatch.xml
```
